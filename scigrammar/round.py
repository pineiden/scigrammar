try:
    from .walnut import Walnut, genpaths, wn2csk
except:
    from walnut import Walnut, genpaths, wn2csk

import regex as re

#compact="(d|e)=(f|g)=h"

def compact_round2walnuts(compact):
    patt=re.compile("\(")
    lround=[m.starts()[0] for m in re.finditer(patt,compact)]
    patt=re.compile("\)")
    rround=[m.starts()[0] for m in re.finditer(patt,compact)]
    rsons=[]
    lsq=list(lround)
    for rb in rround:
        #print(rb)
        dsb=len(compact)
        lb=[q for q in lsq if q<rb][-1]
        #print(lb)
        ds=rb-lb
        pair=(lb,rb)
        rsons.append(pair)
        q=lsq.pop(lsq.index(lb))

    cc=compact
    cb=compact
    wn_sons=[]
    for r in rsons:
        r0=r[0]
        r1=r[1]
        group=cc[r0+1:r1]
        charx='*'
        lc=r1+1-r0
        dlc=lc-len(str(len(wn_sons)))
        #print("DLC")
        #print(dlc)
        compact="%s%s%s%s" % (cb[:r0],charx*dlc,len(wn_sons),cb[r1+1:])
        cb=compact
        #print(compact)
        glist=group.split('|')
        wn=Walnut(glist[0])
        wn.add_leaf(glist[1])
        wn_sons.append(wn)

    clist=compact.split('=')
    for c in clist:
        #print(c)
        #print(c.find('*'))
        if not c.find('*')>-1:
            #print("Ok c")
            wn=Walnut(c)
            wn_sons.append(wn)
    return wn_sons


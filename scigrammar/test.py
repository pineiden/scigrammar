from brackets import parse2wnbank, WnAssemble
from networktools.colorprint import bprint, gprint, rprint
try:
    from .walnut import Walnut, genpaths, wn2csk
except:
    from walnut import Walnut, genpaths, wn2csk

import time

#wnname="APLAPLAC"
wn_d={}
compact=dict(
    a="[%s|[a|[q|[(b|c)=(d|e)]]]=[f|[(g|h)]]]" % "ESTO",
    b="[%s|[(a|b)=(c|d)=(q|g)]]" % "DOTO",
    c="[%s|\
    [a|[b|[c=d]]]= \
    [e|[(f|g)=h]]= \
    [i|[j|[k|[l=m]]]]]" % "PIGRO" )


wn_set=[]
dpaths=[]
for cp in compact:
    rprint(cp)
    wn_bank=parse2wnbank(compact[cp])
    wn=WnAssemble(wn_bank)
    print(wn)
    for w in wn.leaves:
        rprint("Hojas")
        rprint(w)
        if isinstance(w, Walnut):
            for x in w.leaves:
                rprint(x)
                if isinstance(x, Walnut):
                    bprint(x.leaves)
                    for m in x.leaves:
                        gprint("%s->%s" %  (x, m))
                        if isinstance(m, Walnut):
                            rprint("Hojas")
                            bprint(m.leaves)

    gprint("Last leaves")
    #print(wn.leaves[1].leaves[0].leaves)
    #print(wn.leaves[0].leaves[0].leaves)

    dot=wn.createdot()
    code=wn.codeid()
    wn.getgraphviz(code,dot)
    d3graph=wn.getd3graph()
    print(wn.dot.source)
    wn.rendergraph()
    bprint("D3 GRPAH")
    gprint(d3graph)
    bprint(len(d3graph))
    for value in d3graph:
        for (k,va) in value.items():
            rprint("%s-%s" %(k,va))
            if (k=='children'):
                bprint(len(va))
            bprint('-\n')
    jsongraph=wn.getjsongraph()
    gprint(wn)
    bprint("Json dump")
    rprint(jsongraph)
    rprint(wn_bank)
    #time.sleep(4)
    #bprint("Generando walnuts' paths ")
    paths=[]
    paths=genpaths(wn, wn_bank, paths)
    wn.set_paths(paths)
    rprint("Paths")
    dpaths.append(paths)
    #time.sleep(5)
    wn_set.append(wn)
    wn_d[cp]=wn

bprint("End")

for p in dpaths:
    print("Nuevo grafo")
    print(p)

for cp in compact:
    slices=""
    wn=wn_d[cp]
    gprint("Nuestro compact")
    bprint(compact[cp])
    rprint("Nuestro csk")
    csk,wn=wn2csk(slices, wn)
    bprint(csk)

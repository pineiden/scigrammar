try:
    from .walnut import Walnut, genpaths, wn2csk
except:
    from walnut import Walnut, genpaths, wn2csk
import regex as re
try:
    from .lexann import sons_of_sons, get_levels
except:
    from lexann import sons_of_sons, get_levels

try:
    from .round import compact_round2walnuts
except:
    from round import compact_round2walnuts


from networktools.library import my_random_string
from networktools.colorprint import bprint, gprint, rprint

def create_idX(n, idw_list):
    idw=my_random_string(n)
    while idw in idw_list:
        idw=my_random_string(n)
    idw_list.append(idw)
    return (idw_list, idw)

def element(idw,ids):
    return {'pti':ids,'ptw':idw}

def parse2wnbank(compact):
    rc=compact
    patt=re.compile("\[")
    lsquare=[m.starts()[0] for m in re.finditer(patt,compact)]
    patt=re.compile("\]")
    rsquare=[m.starts()[0] for m in re.finditer(patt,compact)]
    idw_list=[]
    n=4
    idw_dot={}
    bsons=[]
    lsq=list(lsquare)
    for rb in rsquare:
        print(rb)
        dsb=len(compact)
        lb=[q for q in lsq if q<rb][-1]
        print(lb)
        ds=rb-lb
        pair=(lb,rb)
        bsons.append(pair)
        q=lsq.pop(lsq.index(lb))
    rltsh=sons_of_sons(bsons)
    levels=get_levels(rltsh)
    #for m,v in enumerate(bsons):
        #print(v,rltsh[m],levels[m])
    uniq_rltsh=list(set(rltsh))
    print("Uniq relationship levels:")
    print(uniq_rltsh)
    this_levels=[]
    for m,v in enumerate(uniq_rltsh):
        (idw_list,idw)=create_idX(n, idw_list)
        this_levels.append(v)
    #print(idw_list)
    ## complete with idw every item in list same size than levels
    idw_brackets=[]
    wn_bank={}
    for m,v in enumerate(rltsh):
        idw_brackets.append(0)
        #idw_list is a list with unique elements
        #in idw_brackets assign the idw to every rltsh by level
        for i,idw in enumerate(idw_list):
            idw_dot[idw]=[]
            wn_bank[idw]={}
            if this_levels[i]==v:
                idw_brackets[m]=idw
    #print("Brackets id")
    #print(idw_brackets)
    #print(rltsh)
    #print(levels)
    #print(idw_dot)
    checkpoints=[0]*len(rltsh)
    #extract data
    #revel different paths--> different walnuts for the forest
    max_level=max(levels)
    #bprint("Maximo nivel en arbol:")
    #print(max_level)
    #print(levels)
    main_tree=None
    cc=compact
    wn_broth=[]
    cb=compact
    #print(levels)
    #print(bsons)
    #Get highest level and split to obtain KEY
    lev_base=levels.index(0)
    interval=bsons[lev_base]
    mainv=compact[interval[0]+1:interval[1]]
    name=mainv.split('|')[0]
    main_tree=Walnut(name)
    while sum(checkpoints)<len(checkpoints):
        #Here mix checkpoints list with levels
        avalaibles=[i for (i,y) in enumerate(checkpoints) if y==0]
        #bprint("Avalaible")
        #bprint(avalaibles)
        #get levels avalaibles:
        av_levels=[x for (i,x) in enumerate(levels) if i in avalaibles ]
        #rprint(av_levels)
        max_level=max(av_levels)
        #ge
        paths=[(x,y) for (x,y) in enumerate(levels) if y>=max_level and x in avalaibles]
        #bprint("Camino a seguir:")
        print(paths)
        for (i, level) in paths:
            nl=rltsh[i]
            same_nl=[(i,x) for (i, x) in enumerate(rltsh) if x==nl and x in avalaibles]
            # do the thing
            # index in bsons and pointer (i,p)
            #bprint("Same NL")
            #rprint(same_nl)
            future={}
            for element in same_nl:
                #Starts with the tail
                e_i=element[0]#index
                e_x=element[1]#pointer
                idw=idw_brackets[e_i]#id level
                #gprint(bsons[e_i])#Pair corresponding that interval  <r0,r1>
                (r0,r1)=bsons[e_i]
                #print(i,max_level)
                slicem=cc[r0+1:r1]# Take string inside [-]
                #print(slicem)
                ids_list=idw_dot[idw]
                #rprint("Round2Walnut")
                wn_set=compact_round2walnuts(slicem)
                for w in wn_set:
                    (ids_list, ids)=create_idX(n, ids_list)
                    w.set_idw(idw)
                    w.set_ids(ids)
                    wn_bank[idw][ids]={'wn':w,'ptw': idw_brackets[e_x]}
                    future[ids]=wn_bank[idw][ids]

                #print(wn_set[1].root, wn_set[1].leaves)
                #print(slicem)
                #print(wn_set)
                #save wn_set on aux list
                #convert compact slice to ***
                #charx='*'
                #lc=r1+1-r0
                #print("Len compact %s" % len(compact))
                #compact="%s%s%s" % (cb[:r0],charx*lc,cb[r1+1:])
                cb=compact
                #print(compact)
                #next level
                # use rltsh
                checkpoints[e_i]=1
                e_i=e_x
                e_x=rltsh[e_x]
                # Then follow the path
                while e_i>=0:
                    rprint("Pair E")
                    #print(e_i,e_x)
                    ####visit every sister on this level
                    ##obtain a list with same nl
                    ##
                    checkpoints[e_i]=1
                    #rprint("New loop")
                    #print(i,e_i)
                    npair=bsons[e_i]
                    idw=idw_brackets[e_i]
                    #print("New pair")
                    #print(npair)
                    #print("prec")
                    #print(compact)
                    r0=npair[0]
                    r1=npair[1]
                    slicem=compact[r0+1:r1]
                    rprint("Slicem")
                    rprint(slicem)
                    slist=slicem.split('|')
                    key=slist[0]
                    k_sons=slist[1].replace('*','')
                    if k_sons.isdigit():
                        sons=int(k_sons)
                        #print("Index %s" % k_sons)
                    wn=Walnut(key)
                    ids_list=idw_dot[idw]
                    (ids_list, ids)=create_idX(n, ids_list)
                    wn.set_ids(ids)
                    wn.set_idw(idw)
                    
                    #add set of previous walnut
                    #gprint("Wn set")
                    #rprint(wn_set)
                    #for w in wn_set:
                    #    wn.add_leaf(w)
                    wn_bank[idw][ids]={'wn':wn,'ptw': idw_brackets[e_x]}                
                    for idf in future:
                        future[idf]['pti']=wn.ids
                    future={}
                    future[ids]=wn_bank[idw][ids]
                    idf=ids
                    #bprint("Nueva raiz")
                    #bprint(wn)
                    #bprint(wn.leaves)
                    wn_set=[wn]
                    #print(rltsh)
                    prnl=nl
                    e_i=e_x
                    e_x=rltsh[e_x]
                    #gprint("Relations")
                    #rprint([e_i,e_x])
                    #gprint(rltsh)
                    #print("%s --> %s" % (prnl,e_i))
                    #create new walnut, with key and sons
                    #sons come from wn_set and k_sons
                    #bprint(e_i)
                    #gprint(wn)
                    if e_x==-1 :                    
                        idw=idw_brackets[e_i]
                        #gprint("Check head tree")
                        #bprint(e_i)
                        #gprint(wn_bank[idw])
                        #rprint(not any(wn_bank[idw]))
                        if not any(wn_bank[idw]):
                            idw_list=idw_dot[idw]
                            (ids_list, ids)=create_idX(n, ids_list)                        
                            #bprint(wn_set[0])
                            #gprint("Añadido")                    
                            #main_tree.add_leaf(wn_set[0])
                            #rprint("Assign main tree")
                            #rprint(ids)
                            main_tree.set_ids(ids)
                            main_tree.set_idw(idw)
                            future[idf]['pti']=main_tree.ids
                            #The main tree at the root
                            wn_bank[idw][ids]={'wn':main_tree,'ptw': None, 'pti':None}
                            #gprint(main_tree)
                            #gprint(main_tree.leaves[0].leaves[0])
                            checkpoints[e_i]=1
                            #close tree
                            #add sons
                        else:
                            #idw_list=idw_dot[idw]
                            #(ids_list, ids)=create_idX(n, ids_list)                        
                            #bprint(wn_set[0])
                            #gprint("Añadido")                    
                            #main_tree.add_leaf(wn_set[0])
                            #rprint("Assign main tree")
                            #rprint(ids)
                            #main_tree.set_ids(ids)
                            #bprint("Main treee ids")
                            #bprint(main_tree.ids)
                            future[idf]['pti']=main_tree.ids
                        break


        #print(paths)
        #print(idw_dot)
        #print(idw_brackets)
        #print(rc)
        #print(main_tree)
        #bprint("Hojas del arbol")
        #print(main_tree.leaves)
        #rprint("Checkpoints")
        #print(checkpoints)
        #print(sum(checkpoints)<len(checkpoints))
        #print(bsons)
        #print(levels)
        #print(rltsh)
        #print(compact)
        #bprint(wn_bank)
        #rprint("Bank")
        #for k in wn_bank:
            #rprint(k)
            #gprint(wn_bank[k])
            #for p in wn_bank[k]:
                #bprint(wn_bank[k][p]['wn'])

    return wn_bank


def WnAssemble(Bank):
    wn=object
    for idw in Bank:
        for ids in Bank[idw]:
            this_wn=Bank[idw][ids]
            #father={'ids':ids, 'idw': idw}
            #wn=this_wn['wn']
            ptw=this_wn['ptw']
            if not 'pti' in this_wn:
                wn=this_wn['wn']
                fti=wn.ids
                fdw=wn.idw
                #son=Bank[idw][ids]['wn']
                #son.set_father(fdw,fti)
            elif this_wn['pti']==None:
                wn=this_wn['wn']
                fti=wn.ids
                fdw=wn.idw
                #son=Bank[idw][ids]['wn']
                #son.set_father(fdw,fti)
            else:
                #print(this_wn)
                pti=this_wn['pti']
                son_wn=this_wn['wn']
                father=Bank[ptw][pti]['wn']
                #this_wn['wn'].set_father(father.idw, father.ids)
                #rprint("Father?")
                #bprint(this_wn['wn'].father)
                father.add_leaf(this_wn['wn'])
    return wn



def create_wn_all(csk):
    wn_bank=parse2wnbank(csk)
    wn=WnAssemble(wn_bank)
    dot=wn.createdot()
    code=wn.codeid()
    wn.getgraphviz(code,dot)
    wn.set_csk(csk)
    paths=[]
    paths=genpaths(wn, wn_bank, paths)
    wn.set_paths(paths)
    return wn

from walnut import Walnut

class WalnutForest:
    def __init__(self,name):
        self.name=name
        self.walnuts=[]

    def add_walnut(self, walnut):
        assert isinstance(walnut, Walnut),'It\'s not a walnut'
        self.walnuts.append(walnut)

    def del_walnut(self, i):
        q=self.walnuts.pop(i)

    def fsize(self):
        return len(self.walnuts)

    def __str__(self):
        return self.name

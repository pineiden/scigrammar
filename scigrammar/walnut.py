from graphviz import Digraph
import networkx as nx
from networkx.readwrite import json_graph

from networktools.colorprint import bprint, gprint, rprint

class Walnut:
    def __init__(self, name):
        self.root=name
        self.idw=None
        self.ids=None
        self.leaves=[]
        self.father={'ids':None,'idw':None}
        self.jsongraph=''
        self.dot=object
        self.size={'height':0,'width':0}
        #self.dot=Digraph(comment=name)

    def set_paths(self, paths):
        self.paths=paths

    def set_idw(self, idw):
        self.idw=idw

    def set_ids(self, ids):
        self.ids=ids

    def set_name(self,name):
        self.root=name

    def add_leaf(self, leaf):
        self.leaves.append(leaf)

    def del_leaf(self, i):
        l=self.leaves.pop(i)

    def set_father(self, idw,ids):
        self.father['idw']=idw
        self.father['ids']=ids

    def wsize(self):
        return len(self.leaves)

    def __str__(self):
        return self.root

    def codeid(self):
        return "%s-%s" % (self.idw,self.ids)

    def createdot(self):
        self.dot=Digraph(name=self.root,
                         node_attr={'shape': 'plaintext'},
                         filename=self.root+".dot",
                         directory='./graphviz',
                         )
        return self.dot

    def getgraphviz(self, code, dot):
        dot.node(code, self.__str__())
        checklist = self.leaves
        for node in checklist:
            if isinstance(node, Walnut):
                nodecode=node.codeid()
                dot.node(nodecode,node.__str__())
                #print(code)
                #print(nodecode)
                dot.edge(code, nodecode)
                checklist=node.leaves
                for node in checklist:
                    if isinstance(node, Walnut):
                        newcode=node.codeid()
                        dot.node(newcode, node.__str__())
                        dot.edge(nodecode, newcode)
                        node.getgraphviz(newcode, dot)
                    elif not isinstance(node,Walnut):
                        dot.node('leaf-%s' % node, node)
                        dot.edge(nodecode, 'leaf-%s' % node)
            elif not isinstance(node, Walnut):
                dot.node('leaf-%s' % node, node)
                dot.edge(code, 'leaf-%s' % node)
        dot.save()

    def getjsongraph(self):
        path='./graphviz/%s.dot' % self.root
        dot_graph=nx.drawing.nx_agraph.read_dot(path)
        self.jsongraph= json_graph.adjacency_data(dot_graph)
        return self.jsongraph

    def getd3graph(self):
        d3_graph={'name':self.__str__(),'children':[]}
        checklist = self.leaves
        for node in checklist:
            if isinstance(node, Walnut):
                d3_graph['children'].append(node.getd3graph())
            elif not isinstance(node, Walnut):
                d3_graph['children'].append(node)
        self.d3=d3_graph
        return d3_graph

    def getgraphs(self):
        return {'dot':self.dot,'json':self.jsongraph,'d3':self.d3}

    def getpath(self, Bank):
        """
        Go deep to from last leaf to root
        """
        leaves=self.leaves
        wn=self
        paths=[]
        element=Bank[wn.idw][wn.ids]
        for leaf in leaves:
            path=[]
            #print(wn)
            while True:
                print(element)
                wn=element['wn']
                key = wn.root
                path.append(key)
                rprint("Bank:")
                gprint(path)
                #print(Bank)
                #print(wn.ids, wn.idw)
                if (not element['pti']==None) and (not element['ptw']==None):
                    #print("Inside")
                    element=Bank[element['ptw']][element['pti']]
                else:
                    break
                #print(path)
                
            path=path[::-1]
            paths.append(path)
        if leaves==[]:
            path=[]
            #print(wn)
            while True:
                #print(element)
                wn=element['wn']
                key = wn.root
                path.append(key)
                #rprint("Bank:")
                #gprint(path)
                #print(Bank)
                #print(wn.ids, wn.idw)
                if (not element['pti']==None) and (not element['ptw']==None):
                    #print("Inside")
                    element=Bank[element['ptw']][element['pti']]
                else:
                    break
                #print(path)
                
            path=path[::-1]
            paths.append(path)


        self.size['width']=len(paths)
        self.size['height']=max(list(map(len,paths)))
        return paths

    def rendergraph(self):
        self.dot.render('walnut_%s' % self.root, view=True)

    def set_csk(self,csk):
        self.csk=csk

#UNCOMPLETE
def wn2csk(slices, wn):
    name=wn.root
    leaves=wn.leaves
    #bprint(leaves)
    if len(leaves)>0:
        #check if not empty
        #rprint(leaves)
        if len(leaves)==1:
            new_slices=""
            for leaf in leaves:
                if isinstance(leaf, Walnut):
                    new_slices, wn=wn2csk(new_slices, leaf)
                    #new_slices += "%s" % new_slices
                    slices="[%s|[%s]]" % (name, new_slices)
                else:
                    new_slices +="%s" % leaf
                    slices="(%s|%s)" % (name, new_slices)
        else:
            new_slices=""
            k=0
            for leaf in leaves:
                if isinstance(leaf, Walnut):
                    new_slices, wn=wn2csk(new_slices, leaf)
                    new_slices += "=%s" % new_slices
                else:
                    new_slices +="=%s" % leaf 
              
            slices="%s|[%s]" % (name, new_slices)
    else:
        slices= "%s" % name

    return slices, wn


def genpaths(wn, bank, paths):
    #for w in bank:
        #gprint("Test bank")
        #print(w, bank[w])
    leaves=wn.leaves
    #gprint("Set of leaves")
    #gprint(leaves)
    for leaf in leaves:
        if isinstance(leaf, Walnut):
            #print(leaf)
            paths=genpaths(leaf, bank, paths)
        else:
            paths_=wn.getpath(bank)
            #bprint(paths_)
            for p in paths_:
                p.append(leaf)
                #rprint(p)
                paths.append(p)
    if leaves==[]:
        paths_=wn.getpath(bank)
        for p in paths_:
            #rprint(p)
            paths.append(p)


    wn.size['width']=len(paths)
    wn.size['height']=max(list(map(len,paths)))

    return paths



from setuptools import setup

setup(name='scigrammar',
      version='0.1',
      description='A module who helps you to manage data in dictionary by its keys',
      url='http://gitlab.com/pineiden/scigrammar',
      author='David Pineda Osorio',
      author_email='dpineda@ug.uchile.cl',
      license='MIT',
      packages=[],
      zip_safe=False)

Install
========

python setup.py develop

SciGrammar Parser
====================


This module can help you to transform a string with a defined grammar
into a Tree. The type of tree is a Walnut. So if you write that text
then you receive a Walnut object.

The main goal of this project is help you to define a Python Dictionary's [https://docs.python.org/3/tutorial/datastructures.html]
structure. Despite to create a new Dictionary class for every different 
type of structure, you can define the keys which your desired dictionary has.

For that you need to put some signs to help the computer to obtain your Walnut.
The signs are:

- |: between keys who are a father and a son (or a group more complex than a son).
- = : between every element at same level and same father.
- [] : [ at the beginning  and ] at the end of a group more complex than only a key.
- () : encircle the last level elements who have sons ie: (a|b).

Features
=========

* Produce a Tree with a Compact String of Keys [CSK]:  CSK->Walnut.
* Obtain a Graphviz code to visualize the tree
* Obtain a file with the tree in a graph
* Obtain every path [TO DO]

How to build?
===============

Check the test.py, brackets.py, lexann.py, round.py and walnut.py files.

Goal
___


Example:

AXY={'A':12,'X':{'V':10,'Err':.2},'Y':{'V':12,'Err':.2}}

has as structure in a CSK way:

CSK(AXY)="[AXY|[A=[X|[(V=Err)]]=[Y|[(V=Err)]]]]"

Then if you have another set of values from same like dictionary, you
can process the same as the original AXY.

So, it's a good idea made a function who extract the keys from some dictionary and
returns the CSK.


Where can i find NetworlTools module?
====================================

git clone https://gitlab.com/pineiden/networktools

pyhton setup.py develop
